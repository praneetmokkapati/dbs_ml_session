* Prerequisties

1. Download and install Anaconda Distribution from https://www.anaconda.com/downloads (Python 3 version)
2. A cursory glance at http://introtopython.org/ is recommended.

* Starting Jupyter Notebook

Go to Anaconda Prompt and Type the command Jupyter Notebook to open the application in your browser.